/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab1;
import java.util.Scanner;
/**
 *
 * @author aof2a
 */
public class Lab1 {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        char[][] Board = {{'-',' ','-',' ','-'},{'-',' ','-',' ','-'},{'-',' ','-',' ','-'}};
        
        String result;
        
        System.out.println("Welcome to Ox");
        while(true){
        printBoard(Board);
        placepeice(Board,"player1");
        result = checkwin(Board,"player1");
        if(result.equals("X win")||result.equals("O win")){
            System.out.println(result);
            break;
        }
            
        
        printBoard(Board);
        placepeice(Board,"player2");
        checkwin(Board,"player2");
        result = checkwin(Board,"player1");
        if(result.equals("X win")||result.equals("O win")){
            System.out.println(result);
            break;
        }
        }
        
    }
    static void printBoard(char[][] Board){
        for(char[] row:Board){
            for(char col: row){
                System.out.print(col);
            }
            System.out.println();

        }
        
    }
    static void placepeice(char[][] Board,String player){
        Scanner kb = new Scanner(System.in);
        char symbol = 0;
        switch (player) {
            case "player1":
                symbol = 'X';
                break;
            case "player2":
                symbol = 'O';
                break;
            default:
                break;
        }
        System.out.println(symbol+" turn");
        System.out.print("Please in put row,col:");
        String pos = kb.next();
        switch (pos) {
            case "00":
                Board[0][0] = symbol;
                break;
            case "01":
                Board[0][2] = symbol;
                break;
            case "02":
                Board[0][4] = symbol;
                break;
            case "10":
                Board[1][0] = symbol;
                break;
            case "11":
                Board[1][2] = symbol;
                break;
            case "12":
                Board[1][4] = symbol;
                break;
            case "20":
                Board[2][0] = symbol;
                break;
            case "21":
                Board[2][2] = symbol;
                break;
            case "22":
                Board[2][4] = symbol;
                break;
            default:
                break;
        }
        
        
    }
    static String checkwin(char[][] Board,String player){
         
        char symbol = 0;
        switch (player) {
            case "player1":
                symbol = 'X';
                break;
            case "player2":
                symbol = 'O';
                break;
            default:
                break;
        }
        
        if (Board[0][0]==Board[0][2]&&Board[0][4]==Board[0][2]&&Board[0][0]!='-'){
           return Board[0][0]+" win";
       }else if(Board[1][0]==Board[1][2]&&Board[1][4]==Board[1][2]&&Board[1][0]!='-'){
           return Board[1][0]+" win";
       }else if(Board[2][0]==Board[2][2]&&Board[2][4]==Board[2][2]&&Board[2][0]!='-'){
           return Board[2][0]+" win";
       }else if(Board[0][0]==Board[1][0]&&Board[2][0]==Board[1][0]&&Board[0][0]!='-'){
           return Board[0][0]+" win";
       }else if(Board[0][2]==Board[1][2]&&Board[2][2]==Board[1][2]&&Board[0][2]!='-'){
           return Board[0][2]+" win";
       }else if(Board[0][4]==Board[1][4]&&Board[2][4]==Board[1][4]&&Board[0][4]!='-'){
           return Board[0][4]+" win";
       }else if(Board[0][0]==Board[1][2]&&Board[2][4]==Board[1][2]&&Board[0][0]!='-'){
           return Board[0][0]+" win";
       }else if(Board[0][4]==Board[1][2]&&Board[2][0]==Board[1][2]&&Board[0][4]!='-'){
           return Board[0][4]+" win";
       }
        return "";
    }
        
        
        
}
